"""
Unit tests for the string utilities module.
"""
import sys
import pytest
from bigpy import trim, ensure_not_blank, u

if sys.version_info < (3,):

        def test_u_with_unicode():
            s = u'I am Uni'
            res = u(s)

            assert s == res
            assert isinstance(res, unicode)


        def test_u_with_regular():
            s = 'I am Plain'
            res = u(s)

            assert s == res
            assert isinstance(s, str)
            assert isinstance(res, unicode)
else:

    def test_u_with_unicode():
        s = 'I am Uni'
        res = u(s)

        assert s == res
        assert isinstance(s, str)
        assert isinstance(res, str)


class TestTrim(object):
    """
    Unit tests for the ```trim``` function
    """
    @pytest.mark.parametrize("value, expected", [
        (None, ''),
        ('      ', ''),
        (' \n \t  ', ''),
        (' \n \t  test1', 'test1'),
        ('test1 \n \t  ', 'test1'),
        ('  test1', 'test1'),
        ('test1  ', 'test1'),
        ('  test1  ', 'test1'),
        (' \n \t  test1 \n \t  ', 'test1')
    ])
    def test_trim_str_implicit(self, value, expected):
        assert trim(value) == expected

    @pytest.mark.parametrize("value, expected", [
        (None, None),
        ('      ', None),
        (' \n \t  ', None),
        (' \n \t  test1', 'test1'),
        ('test1 \n \t  ', 'test1'),
        ('  test1', 'test1'),
        ('test1  ', 'test1'),
        ('  test1  ', 'test1'),
        (' \n \t  test1 \n \t  ', 'test1')
    ])
    def test_trim_str_explicit_none(self, value, expected):
        assert trim(value, None) == expected

    @pytest.mark.parametrize("value, expected", [
        (None, 'stuff'),
        ('      ', 'stuff'),
        (' \n \t  ', 'stuff'),
        (' \n \t  test1', 'test1'),
        ('test1 \n \t  ', 'test1'),
        ('  test1', 'test1'),
        ('test1  ', 'test1'),
        ('  test1  ', 'test1'),
        (' \n \t  test1 \n \t  ', 'test1')
    ])
    def test_trim_str_explicit_stuff(self, value, expected):
        assert trim(value, 'stuff') == expected

    @pytest.mark.parametrize("value, expected", [
        (None, b''),
        (b'      ', b''),
        (b' \n \t  ', b''),
        (b' \n \t  test1', b'test1'),
        (b'test1 \n \t  ', b'test1'),
        (b'  test1', b'test1'),
        (b'test1  ', b'test1'),
        (b'  test1  ', b'test1'),
        (b' \n \t  test1 \n \t  ', b'test1')
    ])
    def test_trim_byte_explicit(self, value, expected):
        assert trim(value, b'') == expected

    @pytest.mark.parametrize("value, expected", [
        (None, None),
        (b'      ', None),
        (b' \n \t  ', None),
        (b' \n \t  test1', b'test1'),
        (b'test1 \n \t  ', b'test1'),
        (b'  test1', b'test1'),
        (b'test1  ', b'test1'),
        (b'  test1  ', b'test1'),
        (b' \n \t  test1 \n \t  ', b'test1')
    ])
    def test_trim_byte_explicit_none(self, value, expected):
        assert trim(value, None) == expected

    @pytest.mark.parametrize("value, expected", [
        (None, 'stuff'),
        (b'      ', 'stuff'),
        (b' \n \t  ', 'stuff'),
        (b' \n \t  test1', b'test1'),
        (b'test1 \n \t  ', b'test1'),
        (b'  test1', b'test1'),
        (b'test1  ', b'test1'),
        (b'  test1  ', b'test1'),
        (b' \n \t  test1 \n \t  ', b'test1')
    ])
    def test_trim_byte_explicit_stuff(self, value, expected):
        assert trim(value, 'stuff') == expected

    @pytest.mark.parametrize("value, expected", [
        (None, ''),
        (b'      ', ''),
        (b' \n \t  ', ''),
        (b' \n \t  test1', b'test1'),
        (b'test1 \n \t  ', b'test1'),
        (b'  test1', b'test1'),
        (b'test1  ', b'test1'),
        (b'  test1  ', b'test1'),
        (b' \n \t  test1 \n \t  ', b'test1')
    ])
    def test_trim_byte_implicit(self, value, expected):
        assert trim(value) == expected

    @pytest.mark.parametrize("value,expected", [
        (u'      ', u''),
        (u' \n \t  ', u''),
        (u' \n \t  test1', u'test1'),
        (u'test1 \n \t  ', u'test1'),
        (u'  test1', u'test1'),
        (u'test1  ', u'test1'),
        (u'  test1  ', u'test1'),
        (u' \n \t  test1 \n \t  ', u'test1')
    ])
    def test_trim_unicode(self, value, expected):
        assert trim(value) == expected


# noinspection PyMethodMayBeStatic
class TestCheckNotBlank(object):
    """
    Unit tests for the ```ensure_not_blank``` function
    """
    def test_value_must_be_non_blank(self):
        test_value = 'some value'
        ensure_not_blank(test_value, 'blah')

    def test_value_must_not_be_none(self):
        test_value = None
        with pytest.raises(ValueError):
            # noinspection PyTypeChecker
            ensure_not_blank(test_value, 'blah')

    def test_value_must_not_be_empty(self):
        test_value = ''
        with pytest.raises(ValueError):
            ensure_not_blank(test_value, 'blah')

    def test_value_must_not_be_all_spaces(self):
        test_value = '   '
        with pytest.raises(ValueError):
            ensure_not_blank(test_value, 'blah')

    def test_value_must_not_be_whitespace(self):
        test_value = ' \n \t  '
        with pytest.raises(ValueError):
            ensure_not_blank(test_value, 'blah')

    def test_message_valid_message(self):
        test_message = 'some message'
        ensure_not_blank('test', test_message)

    def test_message_none(self):
        test_message = None
        # noinspection PyTypeChecker
        ensure_not_blank('test', test_message)

    def test_message_empty(self):
        test_message = ''
        ensure_not_blank('test', test_message)

    def test_message_all_spaces(self):
        test_message = '   '
        ensure_not_blank('test', test_message)

    def test_message_whitespace(self):
        test_message = ' \n \t  '
        ensure_not_blank('test', test_message)

    @pytest.mark.parametrize("value,expected", [
        (' \n \t  test1', 'test1'),
        ('test1 \n \t  ', 'test1'),
        ('  test1', 'test1'),
        ('test1  ', 'test1'),
        ('  test1  ', 'test1'),
        (' \n \t  test1 \n \t  ', 'test1')
    ])
    def test_ensure_not_blank(self, value, expected):
        assert ensure_not_blank(value) == expected

    @pytest.mark.parametrize("value,expected", [
        (b' \n \t  test1', b'test1'),
        (b'test1 \n \t  ', b'test1'),
        (b'  test1', b'test1'),
        (b'test1  ', b'test1'),
        (b'  test1  ', b'test1'),
        (b' \n \t  test1 \n \t  ', b'test1')
    ])
    def test_ensure_not_blank_byte(self, value, expected):
        assert ensure_not_blank(value) == expected

    @pytest.mark.parametrize("value,expected", [
        (u' \n \t  test1', u'test1'),
        (u'test1 \n \t  ', u'test1'),
        (u'  test1', u'test1'),
        (u'test1  ', u'test1'),
        (u'  test1  ', u'test1'),
        (u' \n \t  test1 \n \t  ', u'test1')
    ])
    def test_ensure_not_blank_unicode(self, value, expected):
        assert ensure_not_blank(value) == expected
