"""
Unit tests for the `DotDict` class.
"""
import pytest
from collections import OrderedDict
from bigpy import DotDict


@pytest.fixture
def simple_dict():
    return {
        'test1': 'value1',
        'test2': 2,
        'test3': 3.0,
        '1': 'value-one',
        2: 'value-two'
    }


# noinspection PyShadowingNames
@pytest.fixture
def complex_dict(simple_dict):
    return {
        'key1': 'some key',
        'key2': simple_dict,
        'key3': ['item1', 3, simple_dict],
        'key4': {'elem1', 'elem2', 5},
        'key5': [1, 2, ['a', 'b', simple_dict]]
    }


@pytest.fixture
def dict_raw_list():
    return [
        ('key 1', 'a'),
        ('1', 'b'),
        (3, 'c'),
        ('4.0', 'd'),
        ('  test', 'e'),
        ('test  ', 'f'),
        ('  test  ', 'g'),
        ('5 0', 'h'),
        ('6-0', 'i'),
        ('7.0', 'i1'),
        ('7-0', 'i2'),
        ('8.0', 'j1'),
        (8.0, 'j2'),
        (9.0, 'k1'),
        ('9.0', 'k2'),
        ('some-other value', 'm')
    ]


# noinspection PyShadowingNames
@pytest.fixture
def irregular_keys_dict(dict_raw_list):
    return dict(dict_raw_list)


# noinspection PyShadowingNames
@pytest.fixture
def irregular_keys_ordered_dict(dict_raw_list):
    return OrderedDict(dict_raw_list)


# noinspection PyMethodMayBeStatic,PyShadowingNames
class TestDotDict(object):
    """
    Unit tests for the ```DotDict``` class
    """
    def test_simple_dict(self, simple_dict):
        t = DotDict(simple_dict)
        assert t.test1 == 'value1'
        assert t['test1'] == 'value1'
        assert t.test2 == 2
        assert t.test3 == 3.0
        assert t['1'] == 'value-one'
        assert t[2] == 'value-two'

    def test_simple_dict_with_alt_keys(self, simple_dict):
        t = DotDict(simple_dict, alt_keys=True)
        assert t.test1 == 'value1'
        assert t['test1'] == 'value1'
        assert t.test2 == 2
        assert t.test3 == 3.0
        assert t['1'] == 'value-one'
        assert t[2] == 'value-two'
        assert t.a_1 == 'value-one'
        assert t.a_2 == 'value-two'

    def test_complex_dict(self, complex_dict):
        t = DotDict(complex_dict)
        assert t.key1 == 'some key'

        assert isinstance(t.key2, dict)
        assert isinstance(t.key2, DotDict)
        assert t.key2.test1 == 'value1'
        assert t.key2['test1'] == 'value1'
        assert t['key2']['test1'] == 'value1'

        assert isinstance(t.key3, list)
        assert t.key3[0] == 'item1'
        assert t.key3[1] == 3
        assert t['key3'][0] == 'item1'
        assert t['key3'][1] == 3
        assert isinstance(t.key3[2], dict)
        assert isinstance(t.key3[2], DotDict)
        assert t.key3[2].test1 == 'value1'

        assert isinstance(t.key4, set)
        assert 'elem1' in t.key4

        assert t.key5[0] == 1
        assert t.key5[1] == 2
        assert t.key5[2][0] == 'a'
        assert t.key5[2][1] == 'b'
        assert t.key5[2][2].test1 == 'value1'
        assert t.key5[2][2].test2 == 2

    def test_missing_value(self, simple_dict):
        t = DotDict(simple_dict)
        with pytest.raises(AttributeError):
            print(t.test4)

    def test_irregular_keys(self, irregular_keys_dict):
        t = DotDict(irregular_keys_dict, alt_keys=True)

        assert t.key_1 == 'a'

        assert t.a_1 == 'b'
        assert t['1'] == 'b'

        assert t.a_3 == 'c'
        assert '3' not in t
        assert t[3] == 'c'

        assert t.a_4_0 == 'd'
        assert t['4.0'] == 'd'

        assert t.a__test == 'e'
        assert t.test__ == 'f'
        assert t.a__test__ == 'g'

        assert t.a_5_0 == 'h'
        assert t.a_6_0 == 'i'

        assert t.a_7_0 == 'i1' or t.a_7_0 == 'i2'
        assert t['7.0'] == 'i1'
        assert t['7-0'] == 'i2'

        assert t.a_8_0 == 'j1' or t.a_8_0 == 'j2'
        assert t['8.0'] == 'j1'
        assert t[8.0] == 'j2'

        assert t.a_9_0 == 'k1' or t.a_9_0 == 'k2'
        assert t.a_9_0_ == 'k1' or t.a_9_0_ == 'k2'
        assert t['9.0'] == 'k2'
        assert t[9.0] == 'k1'

        assert t.some_other_value == 'm'

    def test_irregular_keys_ordered_dict(self, irregular_keys_ordered_dict):
        t = DotDict(irregular_keys_ordered_dict, alt_keys=True)

        assert t.key_1 == 'a'
        assert t['key 1'] == 'a'

        assert t.a_1 == 'b'
        assert t['1'] == 'b'

        assert t.a_3 == 'c'
        assert '3' not in t
        assert t[3] == 'c'

        assert t.a_4_0 == 'd'
        assert t['4.0'] == 'd'

        assert t.a__test == 'e'
        assert t.test__ == 'f'
        assert t.a__test__ == 'g'

        assert t.a_5_0 == 'h'
        assert t.a_6_0 == 'i'

        assert t.a_7_0 == 'i1'
        assert t['7.0'] == 'i1'
        assert t['7-0'] == 'i2'

        assert t.a_8_0 == 'j1'
        assert t['8.0'] == 'j1'
        assert t[8.0] == 'j2'

        assert t.a_9_0 == 'k1'
        assert t.a_9_0_ == 'k2'
        assert t['9.0'] == 'k2'
        assert t[9.0] == 'k1'

        assert t.some_other_value == 'm'

    def test_irregular_keys_no_alt_keys(self, irregular_keys_dict):
        t = DotDict(irregular_keys_dict)

        with pytest.raises(AttributeError):
            assert t.key_1 == 'a'

        assert 'a_4_0' not in t
        assert 'a__test__' not in t
        assert 'a_7_0' not in t
        assert 'a_8_0' not in t
        assert 'a_9_0' not in t

        assert t['7.0'] == 'i1'
        assert t['7-0'] == 'i2'

        assert t['8.0'] == 'j1'
        assert t[8.0] == 'j2'

        assert t['9.0'] == 'k2'

    def test_update(self, complex_dict):
        t = DotDict(complex_dict)

        assert t.key1 == 'some key'
        assert t.key2.test1 == 'value1'

        t.key1 = 'mod1'
        t.key2.test1 = 'mod2'

        assert t.key1 == 'mod1'
        assert t['key1'] == 'mod1'
        assert t.key2.test1 == 'mod2'
        assert t.key2['test1'] == 'mod2'
        assert t['key2']['test1'] == 'mod2'

    def test_update_irregular_keys(self, irregular_keys_dict):
        t = DotDict(irregular_keys_dict, alt_keys=True)

        assert t.a__test__ == 'g'
        assert t.some_other_value == 'm'

        t.a__test__ = 'test1'
        t.some_other_value = 'test2'

        assert t.a__test__ == 'test1'
        assert t['  test  '] == 'test1'
        assert t.some_other_value == 'test2'
        assert t['some-other value'] == 'test2'

    def test_delete(self, complex_dict):
        t = DotDict(complex_dict)

        assert t.key1 == 'some key'
        assert t.key2.test1 == 'value1'

        del t.key2.test1

        assert 'test1' not in t.key2
        with pytest.raises(AttributeError):
            print(t.key2.test1)

        del t.key1
        assert 'key1' not in t
        with pytest.raises(AttributeError):
            print(t.key1)

    def test_delete_irregular_keys(self, irregular_keys_dict):
        t = DotDict(irregular_keys_dict, alt_keys=True)

        assert t.key_1 == 'a'
        assert t.a_1 == 'b'

        del t.key_1

        assert 'key 1' not in t
        with pytest.raises(AttributeError):
            print(t.key_1)

        del t['1']
        assert '1' not in t
        with pytest.raises(AttributeError):
            print(t.a_1)

    def test_update_method_simple(self, simple_dict):
        t = DotDict(simple_dict)

        assert t.test1 == 'value1'
        assert t.test2 == 2
        assert t.test3 == 3.0

        t.update({'test1': 77, 'test2': 'a'})

        assert t.test1 == 77
        assert t.test2 == 'a'
        assert t.test3 == 3.0

    def test_update_method_kwargs(self, simple_dict):
        t = DotDict(simple_dict)

        assert t.test1 == 'value1'
        assert t.test2 == 2
        assert t.test3 == 3.0

        t.update(test1=77, test2='a')

        assert t.test1 == 77
        assert t.test2 == 'a'
        assert t.test3 == 3.0

    def test_update_method_simple_and_kwargs(self, simple_dict):
        t = DotDict(simple_dict)

        assert t.test1 == 'value1'
        assert t.test2 == 2
        assert t.test3 == 3.0

        t.update({'test1': 77, 'test2': 'a'}, test3=777)

        assert t.test1 == 77
        assert t.test2 == 'a'
        assert t.test3 == 777

    def test_update_method_dict(self, simple_dict):
        t = DotDict(simple_dict)

        assert t.test1 == 'value1'
        assert t.test2 == 2
        assert t.test3 == 3.0

        t.update(test1={'a': 1, 'b': 3})

        assert isinstance(t.test1, DotDict)
        assert t.test1.a == 1
        assert t.test1.b == 3
        assert t.test2 == 2
        assert t.test3 == 3.0
