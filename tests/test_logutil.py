"""
Unit tests for the logging utilities module.
"""
import pytest
import logging
from bigpy import LoggerProxy


@pytest.mark.parametrize("name", [
        None,
        '',
        '      ',
        ' \n \t  ',
        '\n\t\t\t \n \t  \n\r',
])
def test_name_cannot_be_blank(name):
    with pytest.raises(ValueError):
        LoggerProxy(name)


@pytest.fixture(scope='module', autouse=True)
def config_logging():
    logging.basicConfig()


@pytest.mark.parametrize("value, expected", [
    (' \n \t  test1', 'test1'),
    ('test1 \n \t  ', 'test1'),
    ('  test1', 'test1'),
    ('test1  ', 'test1'),
    ('  test1  ', 'test1'),
    (' \n \t  test1 \n \t  ', 'test1')
])
def test_non_blank_name_is_ok(value, expected):
    logger = LoggerProxy(value)
    assert logger.name == expected


def test_that_debug_method_works():
    logger = LoggerProxy('test')
    logger.debug('Some message')


def test_that_info_method_works():
    logger = LoggerProxy('test')
    logger.info('Some message')


def test_that_error_method_works():
    logger = LoggerProxy('test')
    logger.error('Some message')


def test_that_warn_method_works():
    logger = LoggerProxy('test')
    logger.warn('Some message')


def test_that_bogus_method_raises_error():
    logger = LoggerProxy('test')
    with pytest.raises(AttributeError):
        logger.bogus_level('Some message')
