"""
Unit tests for the JSON utilities module.
"""
import copy
from bigpy import remove_empty_keys

TEST_DICT = {
    'a': None,
    'b': 'test',
    'c': 1,
    'd': 0,
    'e': 0.00,
    'f': '',
    'g': [],
    'h': {
        'a': '',
        'b': 1,
        'c': [
            {
                'a': 1,
                'b': 2
            },
            {
                'a': None,
                'b': 2
            },
            {
                'a': '',
                'b': []
            }
        ]
    },
    'k': {
        'a': '',
        'b': None
    },
    'm': 7
}


def test_remove_empty_keys():
    test_dict = copy.deepcopy(TEST_DICT)
    assert 'a' in test_dict
    assert 'g' in test_dict
    assert 'a' in test_dict['k']

    test_dict = remove_empty_keys(test_dict)

    # assert survivors
    assert 'b' in test_dict
    assert 'c' in test_dict
    assert 'd' in test_dict
    assert 'e' in test_dict
    assert 'h' in test_dict
    assert 'm' in test_dict

    assert 'b' in test_dict['h']
    assert 'c' in test_dict['h']
    assert len(test_dict['h']['c']) == 2
    assert 'a' in test_dict['h']['c'][0]
    assert 'b' in test_dict['h']['c'][0]
    assert 'b' in test_dict['h']['c'][1]

    assert 'a' not in test_dict
    assert 'f' not in test_dict
    assert 'g' not in test_dict
    assert 'a' not in test_dict['h']
    assert 'a' not in test_dict['h']['c'][1]
    assert 'k' not in test_dict





